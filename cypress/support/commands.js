Cypress.Commands.add('fillMandatoryFieldsAndSubmit', function(){
    cy.get('#firstName').type('Anderson');
    cy.get('#lastName').type('Lima');
    cy.get('#email').type('Teste@teste.com');
    cy.get('#open-text-area').type('Teste');
    cy.contains('button', 'Enviar').click();
})